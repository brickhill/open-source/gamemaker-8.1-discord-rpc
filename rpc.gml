#define discord_rpc_initialize
return external_call(global.dll_discord_initialize)

#define discord_rpc_setActivity
return external_call(global.dll_discord_setActivity, argument0)

#define discord_rpc_callback
return external_call(global.dll_discord_callback);

#define discord_rpc_init
export_include_file_location("discord_game_sdk.dll", program_directory + "\discord_game_sdk.dll") //smartlion is god
global.dll_discord_initialize = external_define(temp_directory + "\BrickHillRPC.dll", "initialize", dll_cdecl, ty_real, 0);
global.dll_discord_callback = external_define(temp_directory + "\BrickHillRPC.dll", "callback", dll_cdecl, ty_real, 0);
global.dll_discord_setActivity = external_define(temp_directory + "\BrickHillRPC.dll", "setActivity", dll_cdecl, ty_real, 1, ty_string);
global.dll_discord_clearActivity = external_define(temp_directory + "\BrickHillRPC.dll", "clearActivity", dll_cdecl, ty_real, 0);

#define discord_rpc_clearActivity
return external_call(global.dll_discord_clearActivity)


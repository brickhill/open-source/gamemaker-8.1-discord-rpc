# GameMaker 8.1 Discord RPC

A GameMaker 8.1 extension that lets you display the name of a game on Discord.

![](https://cdn.discordapp.com/attachments/809904816867901500/876108928612712458/unknown.png)

**Note**: this project was primarily made to work for the Brick Hill legacy client so it may require slight modification to work for your needs.

## Instructions

Follow everything step-by-step here:

https://discord.com/developers/docs/game-sdk/sdk-starter-guide#step-0-some-notes

Once you are done creating your application (and adding the oath redirect) navigate to this page:

https://discord.com/developers/docs/game-sdk/sdk-starter-guide#code-primer-no-engine-cpp

Follow the instructions listed above and compile the dllmain.cpp (after modifying the client id and presence data inside of the code) with the discord Game SDK source files (cpp and header files) and include discord_game_sdk.dll.lib to the linker.

Once you compile the dll you will need to include both the dll you have created and the discord_game_sdk.dll (from the Game SDK) into GameMaker. Once you have included both, you should import rpc.gml listed in this library and rename the dll calls in the gml scripts to the name of your dll. 

When everything is loaded you can call the following functions:

- **discord_rpc_initialize()** // Initialize the RPC client, it will return 0 or 1 indicating if it's successful.
- **discord_rpc_callback()** // You will want to run this on every frame, it will return 0 or 1 indiciating if it's successful. You should only call this after a successful initialization.
- **discord_rpc_setActivity(string)** // This will set the status.
- **discord_rpc_clearActivity()** // This will clear the status.
